import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ModalModule } from 'ngx-bootstrap/modal';
import { JwtModule } from '@auth0/angular-jwt';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MenuBarComponent } from './menu-bar/menu-bar.component';
import { MapComponent } from './map/map.component';
import { ListComponent } from './list/list.component';
import { MapViewComponent } from './map-view/map-view.component';
import { ListViewComponent } from './list-view/list-view.component';
import { PosterDialogComponent } from './poster-dialog/poster-dialog.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AdminViewComponent } from './admin-view/admin-view.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AccountViewComponent } from './account-view/account-view.component';
import { AboutComponent } from './about/about.component';

const jwtModuleOptions = {
  config: {
    tokenGetter: localStorage.token,
    whitelistedDomains: ['localhost:8000']
  }
}

@NgModule({
  declarations: [
    AppComponent,
    MenuBarComponent,
    MapComponent,
    ListComponent,
    MapViewComponent,
    ListViewComponent,
    PosterDialogComponent,
    LoginComponent,
    LogoutComponent,
    AdminViewComponent,
    SignUpComponent,
    AccountViewComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    HttpClientModule,
    LeafletModule.forRoot(),
    ModalModule.forRoot(),
    JwtModule.forRoot(jwtModuleOptions)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
