import { Component, OnInit, OnDestroy } from '@angular/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { latLng, tileLayer, marker, Marker, Map } from 'leaflet';
import { PosterDialogComponent } from '../poster-dialog/poster-dialog.component';
import { Poster } from '../poster';
import { MapService } from '../map.service';

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  private map: Map;

  constructor(
    private mapService: MapService
  ) { }

  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; OpenStreetMap contributors'
      })
    ],
    zoom: 7,
    center: latLng([49.3987524, 8.6724335])
  };

  layers = [];
  ngOnInit() {
  }

  onNewPoster(poster: Poster) {
    this.layers.push(poster.getMarker());
  }

  onMapReady(map: Map) {
    this.map = map;
    this.map.setView(this.mapService.location, this.mapService.zoom);
    console.log(map);
  }

  onMapZoom(event) {
    if (this.map) {
      this.mapService.zoom = this.map.getZoom();
    }
  }

  onMapMove(event) {
    if (this.map) {
      this.mapService.location = this.map.getCenter();
    }
  }

}
