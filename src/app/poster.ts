import { LatLngExpression, Marker, marker, Icon, IconOptions } from 'leaflet';
import { MapService } from './map.service';

export class Poster {
    public id: number;
    public location: LatLngExpression;
    public pictureUrl: string;
    public comment: string;
    public added: Date;
    public removeBy: Date = new Date(1558828800);

    public getMarker(): Marker {
        let now: Date = new Date();
        let daysToDeadline = now.getDay() - this.removeBy.getDay();
        return marker(this.location, {
            icon: new Icon({
                iconUrl: this.getIconUrl(daysToDeadline),
                iconSize: [40, 40]
            })
        });
    }

    public from({ id, location, picture_url, comment, added, remove_by }): Poster {
        let poster = new Poster();
        poster.id = id;
        poster.location = location;
        poster.pictureUrl = picture_url;
        poster.comment = comment;
        poster.added = new Date(added);
        poster.removeBy = new Date(remove_by);
        return poster;
    }

    private getIconUrl(daysToDeadline: number): string {
        if (daysToDeadline <= 1) {
            return 'assets/img/location-marker-red.png';
        } else if (daysToDeadline <= 3) {
            return 'assets/img/location-marker-orange.png';
        } else {
            return  'assets/img/location-marker-green.png';
        }
    }
}
