import { Injectable } from '@angular/core';
import { LatLngExpression } from 'leaflet';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor() { }

  getLocation(): Promise<LatLngExpression> {
    return new Promise(function (resolve, reject) {
      navigator.geolocation.getCurrentPosition(function (position) {
        resolve([position.coords.latitude, position.coords.longitude]);
      }, function (error) {
        reject(error.message);
      })
    })
  }

}
