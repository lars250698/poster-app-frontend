import { JwtHelperService } from '@auth0/angular-jwt';

export class User {
    public id: number;
    public name: string;
    public admin: boolean;

    static from (token: string) {
        try {
            let decodedToken = new JwtHelperService().decodeToken(token);
            return new User(decodedToken);
        } catch (_) {
            return null;
        }
    }

    constructor ({ sub, name, admin}) {
        this.id = sub;
        this.name = name;
        this.admin = admin;
    }
}
