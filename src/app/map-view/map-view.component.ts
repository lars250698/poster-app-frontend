import { Component, OnInit } from '@angular/core';
import { MapComponent } from '../map/map.component'

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss']
})
export class MapViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
