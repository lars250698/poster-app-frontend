import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserAuthenticatedGuardService implements CanActivate {

  constructor(
    private authService: AuthService, 
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.user === null) {
      this.authService.redirectToLogIn(state.url);
      return false;
    }
    if (!this.authService.isTokenValid()) {
      alert('Token expired');
      this.authService.redirectToLogOut();
      return false;
    }
    return true;
  }
}
