import { Injectable } from '@angular/core';
import { User } from './user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private jwtHelper = new JwtHelperService();
  public user = User.from(localStorage.token);

  constructor(
    private router: Router
  ) { }

  isTokenValid() {
    try {
      return !this.jwtHelper.isTokenExpired(localStorage.token);
    } catch (_) {
      return false;
    }
  }

  logIn() {
    this.user = User.from(localStorage.token);
  }

  logOut() {
    this.user = null;
  }

  redirectToLogOut() {
    this.router.navigate(['/logout']);
  }

  redirectToLogIn(redirect?: string) {
    let params = {};
    if (redirect) {
      params = {
        queryParams: {
          redirect: redirect
        }
      }
    }
    this.router.navigate(['/'], params);
  }
}
